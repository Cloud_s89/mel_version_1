﻿using UnityEngine;
using UnityEditor;

class ProceduralMesh
{
	static Vector3[] slopeVertices = new Vector3[]{
		new Vector3(-0.5f, -0.5f, -0.5f),
		new Vector3(0.5f, -0.5f, -0.5f),
		new Vector3(0.5f, -0.5f, 0.5f),
		new Vector3(-0.5f, -0.5f, 0.5f),

		new Vector3(-0.5f, -0.5f, -0.5f),
		new Vector3(0.5f, -0.5f, -0.5f),
		new Vector3(0.5f, 0.5f, -0.5f),

		new Vector3(0.5f, -0.5f, 0.5f),
		new Vector3(-0.5f, -0.5f, 0.5f),
		new Vector3(0.5f, 0.5f, 0.5f),

		new Vector3(-0.5f, -0.5f, -0.5f),
		new Vector3(0.5f, 0.5f, -0.5f),
		new Vector3(0.5f, 0.5f, 0.5f),
		new Vector3(-0.5f, -0.5f, 0.5f),

		new Vector3(0.5f, -0.5f, -0.5f),
		new Vector3(0.5f, -0.5f, 0.5f),
		new Vector3(0.5f, 0.5f, 0.5f),
		new Vector3(0.5f, 0.5f, -0.5f),
	};

	static int[] slopeTriangles = new int[]{
		0, 1, 2,
		0, 2, 3,
		4, 6, 5,
		8, 7, 9,
		10, 12, 11,
		10, 13, 12,
		14, 17, 15,
		15, 17, 16,
	};

	[MenuItem("Assets/CreateSlope")]
	static void makeSlope () 
	{
		Mesh mesh = new Mesh();
		//Mesh mesh = obj.GetComponent<MeshFilter>().sharedMesh;
		mesh.Clear ();
		mesh.vertices = slopeVertices;
		mesh.triangles = slopeTriangles;
		mesh.RecalculateNormals ();

		AssetDatabase.CreateAsset (mesh, "Assets/Slope.asset");
	}
}