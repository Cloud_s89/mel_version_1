﻿using UnityEngine;
using System.Collections;
//--------------------------------------------------
public class PlayerController : MonoBehaviour
{
	//Character States
	public enum STATE {IDLE=0,WALK=1};

	public STATE CurrentState
	{
		get
		{
			return m_CurrentState;
		}

		set
		{
			if (m_CurrentState == value)
				return;

			m_CurrentState = value;
			StopAllCoroutines ();

			switch(m_CurrentState)
			{
				case STATE.IDLE:
				StartCoroutine (StateIdle());
				return;

				case STATE.WALK:
				StartCoroutine (StateWalk());
				return;
			}
		}
	}

	public float IdleTimeElapsed = 0f;
	public float TotalIdleTime = 5f;

	private Animator ThisAnimator = null;
	public STATE m_CurrentState = STATE.IDLE;
	//--------------------------------------------------
	void Awake()
	{
		ThisAnimator = GetComponentInChildren<Animator> ();
	}
	//--------------------------------------------------
	// Use this for initialization
	void Start () 
	{
		GetComponent<MoveCharacter>().Init();
		CurrentState = STATE.IDLE;
		StartCoroutine (StateIdle());
	}
	//--------------------------------------------------
	#region IdleState
	public IEnumerator StateIdle()
	{
		IdleTimeElapsed = 0;
		ThisAnimator.SetFloat("IdleTime", IdleTimeElapsed);
		ThisAnimator.SetInteger ("State", (int)m_CurrentState);

		while(CurrentState == STATE.IDLE)
		{
			

			//Wait for next frame
			yield return null;

			IdleTimeElapsed += Time.deltaTime;
			ThisAnimator.SetFloat ("IdleTime", IdleTimeElapsed);

			if (IdleTimeElapsed > TotalIdleTime)
				IdleTimeElapsed = 0;
		}
	}
	#endregion
	//--------------------------------------------------
	#region WalkState
	public IEnumerator StateWalk()
	{
		ThisAnimator.SetInteger ("State", (int)m_CurrentState);

		while (CurrentState == STATE.WALK)
		{
			//Wait for next frame
			yield return null;
		}
	}
	#endregion
	//--------------------------------------------------
	//This is called when the character moves
	public void OnCharacterMove()
	{
		CurrentState = STATE.WALK;
	}
	//--------------------------------------------------
	public void OnCharacterStop()
	{
		CurrentState = STATE.IDLE;
	}
	//--------------------------------------------------
}
