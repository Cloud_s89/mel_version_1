﻿using UnityEngine;
using System.Collections;

public class MenuButtonDisplay : MonoBehaviour 
{
	//Reference to UI level buttons
	public GameObject[] Buttons;

	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i<=GameController.HighestReachedLevel; i++) 
			Buttons [i].SetActive (true);
		
	}
}
