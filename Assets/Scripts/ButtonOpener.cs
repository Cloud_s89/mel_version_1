﻿using UnityEngine;
using System.Collections;

public class ButtonOpener : MonoBehaviour {

	public Animator animator;
	// Use this for initialization
	void Awake () 
	{
		animator = GetComponent <Animator> ();
	
	}


	public void ToggleBool()
	{
		bool AnimBool = animator.GetBool ("OpenPanel");

		animator.SetBool ("OpenPanel", !AnimBool);
	}


}
