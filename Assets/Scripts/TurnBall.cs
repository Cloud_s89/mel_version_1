﻿using UnityEngine;
using System.Collections;

public class TurnBall : MonoBehaviour
{
	public bool SphereActivated = false;
	public bool ShowSphere = false;

	private Animator ThisAnimator = null;

	// Use this for initialization
	void Awake () 
	{
		ThisAnimator = GetComponent<Animator> ();
	}

	public void ActivateSphere(bool SphereActivate)
	{
		//Should sphere be activated?
		SphereActivated = SphereActivate;
	}

	public void ShowSphereObject(bool HideShow)
	{
		if (!SphereActivated)
			return;

		ShowSphere = HideShow;
		ThisAnimator.SetBool ("SphereShow", ShowSphere);
	}
}
